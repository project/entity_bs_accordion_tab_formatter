<?php

namespace Drupal\entity_bs_accordion_tab_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Logger\LoggerChannelTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Plugin implementation of the 'entity_bs_accordion_tab_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_bs_accordion_tab_formatter",
 *   label = @Translation("Entity Bootstrap Accordion Tab formatter"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class EntityBSAccordionTabFormatter extends FormatterBase {
  use LoggerChannelTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  final public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, EntityFieldManagerInterface $entityFieldManager, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $language_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
    // Implement default settings.
      'fields' => '',
      'style' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Implements the settings form.
    $field_settings = $this->getFieldSettings();
    $entity_type_id = $field_settings['target_type'];
    $bundles = $field_settings['handler_settings']['target_bundles'];
    $fields_style = array_keys($this->getEntityFields($form['#entity_type'], $form['#bundle']));

    $elements['entity_type_id'] = [
      '#value' => $entity_type_id,
    ];
    foreach ($bundles as $bundle) {
      $fields = array_keys($this->getEntityFields($entity_type_id, $bundle));
      $fields = array_combine($fields, $fields);
      $elements['fields'][$bundle] = [
        '#type' => 'fieldset',
        '#title' => $bundle,
      ];
      $elements['fields'][$bundle]['tab_title'] = [
        '#type' => 'select',
        '#options' => $fields,
        '#title' => $this->t('Select the title field.'),
        '#default_value' => $this->getSetting('fields')[$bundle]['tab_title'],
        '#required' => TRUE,
      ];
      $elements['fields'][$bundle]['tab_body'] = [
        '#type' => 'select',
        '#options' => $fields,
        '#title' => $this->t('Select the body field.'),
        '#default_value' => $this->getSetting('fields')[$bundle]['tab_body'],
      ];
    }

    $fields_style = array_combine($fields_style, $fields_style);
    $elements['style'] = [
      '#type' => 'select',
      '#options' => $fields_style,
      '#title' => $this->t('Select the display style field.'),
      '#default_value' => $this->getSetting('style'),
    ];
    return $elements + parent::settingsForm($form, $form_state);
  }

  /**
   * Get fields from entity bundle.
   *
   * @param string $entity_type_id
   *   Entity type machine name.
   * @param string $bundle
   *   Entity type bundle machine name.
   */
  private function getEntityFields($entity_type_id, $bundle) {
    $fields = [];
    if (!empty($entity_type_id)) {
      $fields = array_filter(
        $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle), function ($field_definition) {
            return $field_definition instanceof FieldConfig;
        }
      );
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $logger = $this->getLogger('entity_bs_accordion_tab_formatter');
    try {
      $config = $this->configFactory->get('entity_bs_accordion_tab_formatter.settings');
      $bs = $config->get('bootstrap_version');
      $elements = [];
      $style_field = $this->getSetting('style');
      $entity_type_id = $this->getFieldSettings()['target_type'];
      $style = $component_id = '';
      if ($parent = $items->getParent()) {
        $content_parent = $parent->getValue();
        $style = $content_parent->get($style_field)->getValue()[0]['value'];
        $component_id = $style . $content_parent->get('id')->getValue()[0]['value'];
      }
      $tabs = [];
      $first = TRUE;
      foreach ($items as $item) {
        $random = new Random();
        $id = $item->getValue()['target_id'] ?? $random->name(8, TRUE);
        if ($id) {
          $content = $this->entityTypeManager->getStorage($entity_type_id)->load($id) ?? $item->getValue()['entity'];

          if ($language_id = $this->languageManager->getCurrentLanguage()->getId()) {
            if ($content->getTranslation($language_id)) {
              $content = $content->getTranslation($language_id);
            }
          }

          $title = NULL;
          $body = NULL;
          if (method_exists($content, 'get')) {
            $bundle = $content->bundle();

            $title_field = $this->getSetting('fields')[$bundle]['tab_title'];
            $body_field = $this->getSetting('fields')[$bundle]['tab_body'];

            $title = $content->get($title_field)->getValue()[0]['value'];
            $view_mode = 'default';
            if ($this->moduleHandler->moduleExists('view_mode_selector')) {
              $bundle_fields = $this->getEntityFields($entity_type_id, $bundle);
              foreach ($bundle_fields as $field_name => $field_instance) {
                if ($field_instance->getType() == 'view_mode_selector') {
                  $view_mode = $content->get($field_name)->getValue()[0]['value'];
                }
              }
            }

            if (isset($content->get($body_field)->getValue()[0]['format'])) {
              $body = check_markup($content->get($body_field)->getValue()[0]['value'], $content->get($body_field)->getValue()[0]['format']);
            }
            else {
              $body = $content->get($body_field)->view($view_mode);
            }
          }

          switch ($style) {
            case 'tab':
              $theme = 'entity_bs_tab_formatter';
              if ($bs == 'bs4') {
                $li_attributes = [
                  'class' => ['nav-item'],
                ];
                $header_attributes = [
                  'role' => 'tab',
                  'data-toggle' => 'tab',
                  'href' => '#' . $style . $id,
                  'aria-controls' => $style . $id,
                  'class' => $first ? ['nav-link', 'active'] : ['nav-link'],
                ];
              }
              elseif ($bs == 'bs5') {
                $li_attributes = [
                  'class' => ['nav-item'],
                ];
                $header_attributes = [
                  'type' => 'button',
                  'role' => 'tab',
                  'data-bs-toggle' => 'tab',
                  'data-bs-target' => '#' . $style . $id,
                  'id' => $style . $id . '-tab',
                  'aria-controls' => $style . $id,
                  'class' => $first ? ['nav-link', 'active'] : ['nav-link'],
                  'aria-selected' => $first ? 'true' : 'false',
                ];
                $h2_attr_responsive = [
                  'class' => ['accordion-header', 'd-lg-none'],
                  'id' => $style . $id . '-accordion',
                ];
                $button_attr_responsive = [
                  'type' => 'button',
                  'data-bs-toggle' => 'collapse',
                  'data-bs-target' => '#accordion-content' . $style . $id,
                  'aria-expanded' => $first ? 'true' : 'false',
                  'aria-controls' => 'accordion-content' . $style . $id,
                  'class' => ['accordion-button'],
                ];
                $body_attributes_responsive = [
                  'id' => 'accordion-content' . $style . $id,
                  'class' => $first ? ['accordion-collapse', 'collapse', 'd-lg-block', 'show'] : ['accordion-collapse', 'collapse', 'd-lg-block'],
                  'aria-labelledby' => $style . $id . '-accordion',
                ];
              }
              else {
                $li_attributes = [
                  'role' => 'presentation',
                  'class' => $first ? ['active'] : NULL,
                ];
                $header_attributes = [
                  'role' => 'tab',
                  'data-toggle' => 'tab',
                  'href' => '#' . $style . $id,
                  'aria-controls' => $style . $id,
                ];
              }
              $body_attributes = [
                'role' => 'tabpanel',
                'class' => $first ? ['tab-pane', 'active'] : ['tab-pane'],
                'id' => $style . $id,
                'aria-labelledby' => $style . $id . '-tab',
              ];
              break;

            case 'accordion':
            case 'accordion_closed':
            default:
              $theme = 'entity_bs_accordion_formatter';
              $li_attributes = [];
              $header_attributes = [
                'aria-expanded' => $first && $style != 'accordion_closed' ? 'true' : 'false',
                'data-toggle' => 'collapse',
                'data-parent' => '#' . $component_id,
                'aria-controls' => 'c' . $style . $id,
              ];
              $body_attributes = [
                'id' => 'c' . $style . $id,
                'class' => [
                  'collapse',
                ],
                'role' => 'tabpanel',
                'aria-labelledby' => $style . $id,
                'data-parent' => '#' . $component_id,
              ];
              if ($bs == 'bs4') {
                $header_attributes['data-target'] = '#c' . $style . $id;
                $body_attributes['class'][] = $first && $style != 'accordion_closed' ? 'in show' : '';
              }
              elseif ($bs == 'bs5') {
                unset($header_attributes['data-toggle'], $header_attributes['data-parent'], $body_attributes['data-parent']);
                $header_attributes['data-bs-toggle'] = 'collapse';
                $header_attributes['data-bs-target'] = '#c' . $style . $id;
                $header_attributes['class'][] = 'accordion-button';
                $header_attributes['class'][] = !$first || $style == 'accordion_closed' ? 'collapsed' : '';
                $body_attributes['data-bs-parent'] = '#' . $component_id;
                $body_attributes['class'][] = 'accordion-collapse';
                $body_attributes['class'][] = $first && $style != 'accordion_closed' ? 'show' : '';
              }
              else {
                $header_attributes['href'] = '#c' . $style . $id;
                $body_attributes['class'][] = 'panel-collapse';
                $body_attributes['class'][] = $first && $style != 'accordion_closed' ? 'in' : '';
              }
              break;
          }

          $tabs[$id] = [
            'id' => $style . $id,
            'li_attributes' => $li_attributes,
            'header_attributes' => $header_attributes,
            'body_attributes' => $body_attributes,
            'title' => $title,
            'body' => $body,
            'content' => $content,
          ];

          $tabs[$id]['h2_attr_responsive'] = isset($h2_attr_responsive) ?  $h2_attr_responsive : '';
          $tabs[$id]['button_attr_responsive'] = isset($button_attr_responsive) ?  $button_attr_responsive : '';
          $tabs[$id]['body_attributes_responsive'] = isset($body_attributes_responsive) ?  $body_attributes_responsive : '';

          $first = FALSE;
        }
      }
      switch ($style) {
        case 'tab':
          $theme = 'entity_bs_tab_formatter';
          break;

        case 'accordion':
        default:
          $theme = 'entity_bs_accordion_formatter';
          break;
      }
      $elements[0] = [
        '#theme' => $theme,
        '#tabs' => $tabs,
        '#attributes' => [
          'id' => $component_id,
        ],
      ];
      if ($style == 'tab' && $bs == 'bs3') {
        $elements[0]['#attached']['library'][] = 'entity_bs_accordion_tab_formatter/bootstrap-responsive-tabs';
        $elements[0]['#attached']['library'][] = 'entity_bs_accordion_tab_formatter/tabs';
      }

      if ($style == 'tab' && $bs == 'bs5') {
        $elements[0]['#attached']['library'][] = 'entity_bs_accordion_tab_formatter/tabs';
      }

      return $elements;
    }
    catch (\Exception $e) {
      $logger->warning($e->getMessage());
    }
    return [];
  }

}
