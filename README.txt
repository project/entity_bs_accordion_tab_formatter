CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Entity Ref Bootstrap Accordion / Tab Formatter

Simple Bootstrap Accordion / Tab Field formatter for field types
"entity_reference , entity_reference_revisions".
So it will be available Paragraphs module as well.

It's a fork to https://www.drupal.org/project/entity_ref_tab_formatter

No library used except Bootstrap.

Use of 3 fields including a type field that has as possible value:
- accordion | Accordion with first open
- accordion_closed | Accordion closed
- tab | Tabs

REQUIREMENTS
------------

This module requires the following library:

 * Bootstrap (https://getbootstrap.com/)
 * Bootstrap Response Tabs (https://github.com/openam/bootstrap-responsive-tabs)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure Bootstrap Version on admin/config/user-interface/entity-bs
 * Configure the formatter on Display Settings
